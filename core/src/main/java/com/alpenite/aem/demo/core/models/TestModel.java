package com.alpenite.aem.demo.core.models;

import com.day.cq.wcm.api.Page;
import org.apache.sling.api.SlingHttpServletRequest;
import org.apache.sling.api.resource.Resource;
import org.apache.sling.models.annotations.DefaultInjectionStrategy;
import org.apache.sling.models.annotations.Model;

import javax.annotation.PostConstruct;
import javax.inject.Inject;
import java.util.ArrayList;
import java.util.List;

@Model(adaptables = {SlingHttpServletRequest.class, Resource.class}, defaultInjectionStrategy = DefaultInjectionStrategy.OPTIONAL)
public class TestModel {
    /*    @Inject
        private String text;
    */
    @Inject
    private Page currentPage;
    private List<Page>  pageList=new ArrayList<>();

    private Page pageParent;


    @PostConstruct
    protected void activate() {

        MyPath myPath = new MyPath(currentPage.getName().toString(), currentPage.getPath().toString());
        pageParent=currentPage;
        pageList.add(currentPage);
        for(int i=currentPage.getDepth();i>2;i--)
        {

            pageParent=pageParent.getParent();
            pageList.add(pageParent);
        }
    /* System.out.println("sono nel model");
        if (StringUtils.isEmpty(text))
        {
          text="text is null";
        }
        else
        {
            text=text.replaceAll(" ","-");
        }*/
    }

    public List<Page> pageList() {

        return pageList;

    }

   /* public String getText()
    {
        return text;
    }*/
}
