package com.alpenite.aem.demo.core.models;

import java.util.ArrayList;
import java.util.List;

public class MyPath {
    String name;
    String path;



    public  MyPath(String name,String path)
    {
        this.name=name;
        this.path=path;

    }

    public String getName() {
        return name.toString();
    }

    public String getPath() {
        return path;
    }

    @Override
    public String toString() {
        return "MyPath{" +
                "name='" + name + '\'' +
                ", path='" + path + '\'' +
                '}';
    }
}
